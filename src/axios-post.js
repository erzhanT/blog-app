import axios from "axios";

const axiosPost = axios.create({
    baseURL: 'https://blog-erzhan-default-rtdb.firebaseio.com/'
});

export default axiosPost;
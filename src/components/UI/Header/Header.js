import React from 'react';
import {NavLink} from 'react-router-dom';

import './Header.css'

const Header = () => {
    return (
        <div className={'nav'}>
            <h2 className="name">My Blog</h2>
            <div className="main-item">
                <div className={'item'}>
                    <NavLink className="link" to={'/'}>Post</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/add'}>Add</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/contacts'}>Contacts</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/about'}>About</NavLink>
                </div>

            </div>

        </div>
    );
};

export default Header;
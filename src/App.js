import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Add from "./containers/Add/Add";
import './App.css';
import Post from "./containers/Post/Post";
import Header from './components/UI/Header/Header'
import AboutPost from "./containers/AboutPost/AboutPost";
import Edit from "./containers/Edit/Edit";
import Contacts from "./containers/Contacts/Contacts";
import About from "./containers/About/About";

function App() {



  return (
      <>
        <Header/>
        <Switch>
          <Route path={'/'} exact component={Post}/>
          <Route path={'/add'} exact component={Add}/>
          <Route path={'/post/:id'} exact component={AboutPost}/>
          <Route className="edit" path={'/post/:id/edit'} exact component={Edit}/>
          <Route path={'/contacts'} exact component={Contacts}/>
          <Route path={'/about'} exact component={About}/>
        </Switch>
      </>
  );
}

export default App;

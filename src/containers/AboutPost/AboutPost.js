import React, {useEffect, useState} from 'react';
import axiosPost from "../../axios-post";
import {NavLink} from "react-router-dom";
import './AboutPost.css';
import moment from "moment";

const AboutPost = (props) => {
    const [postData, setPostData] = useState({});
    const id = props.match.params.id

    useEffect(() => {
        axiosPost.get(`/post/${id}.json`).then(response => {
            setPostData(response.data);
        }, e => {
            console.log(e);
        })
    }, [id]);



    const removePost = () => {
        axiosPost.delete(`/post/${id}.json`).then(()=> {
            props.history.push('/');
        })
    }

    return (
        <div className="box">
            <p>{moment(postData.date).format('MMMM Do YYYY, hh:mm:ss a')}</p>
            <p>{postData.title}</p>
            <p>{postData.text}</p>



            <NavLink to={`/post/${id}/edit`}>Edit</NavLink>
            <button className="remove" onClick={removePost}>Delete</button>
        </div>
    );
};

export default AboutPost;
import React, {useState, useEffect} from 'react';
import axiosPost from "../../axios-post";
import './Edit.css'

const Edit = (props) => {
    const [edit, setEdit] = useState({});
    const id = props.match.params.id

    const onChange = (e) => {

        const {name, value} = e.target;
        setEdit(prevState => ({
            ...prevState,
            [name]: value
        }));
    }
    useEffect(() => {
        axiosPost.get(`/post/${id}.json`).then(response => {
           setEdit(response.data);
        });
    }, [id])

    const editPost = () => {
        const data = {
            title: edit.title,
            date: edit.date,
            text: edit.text
        }
        axiosPost.put(`/post/${id}.json`, data).then(() => {
            props.history.push('/');
        })
    }

    return (
        <div>
            <input type="text"
                   value={edit.title}
                   name="title"
                   onChange={(e) => onChange(e)}
            />
            <textarea
                cols="30"
                rows="10"
                value={edit.text}
                name="text"
                onChange={(e) => onChange(e)}
            />
            <button className="edit" onClick={editPost}>Save</button>
        </div>
    );
};

export default Edit;
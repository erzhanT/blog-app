import React, {useState} from 'react';
import axiosPost from "../../axios-post";
import './Add.css';


const Add = props => {
    const [addPost, setAddPost] = useState({
        title: '',
        text: ''
    });

    const postDataChanged = event => {
        const {name, value} = event.target;
        setAddPost(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const postHandler = async event => {
        event.preventDefault();

        const obj = {
            date: new Date(),
            title: addPost.title,
            text: addPost.text
        }
        try {
            await axiosPost.post('/post.json', obj)
        } finally {
            props.history.push('/');
        }
    };
    return (
        <div className="Add">
            <h2 className="add-title">Add new post</h2>
            <form className="Submit" onSubmit={postHandler}>
                <h5>Title:</h5>
                <input
                    type="text"
                    placeholder="Title"
                    name="title"
                    value={addPost.title}
                    onChange={postDataChanged}
                    required
                />
                <h5>Description:</h5>
                <textarea
                    name="text"
                    cols="30" rows="10"
                    placeholder="Text"
                    value={addPost.text}
                    onChange={postDataChanged}
                    required
                />
                <button className='save'>Save</button>
            </form>
        </div>
    );
};

export default Add;
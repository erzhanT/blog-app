import React, {useState, useEffect} from 'react';
import axiosPost from "../../axios-post";
import moment from 'moment';
import {NavLink} from "react-router-dom";

import './Post.css';

const Post = () => {

    const [posts, setPosts] = useState('');

    useEffect(() => {
        getPost();

    }, [])

    const getPost = () => {
        axiosPost.get('/post.json').then(result => {
            setPosts(result.data)
        }, e => {
            console.log(e);
        });
    };

    const checkPost = () => {
        if (!posts) {
            return <h1>None posts</h1>
        } else {
            return (
                <>
                    {Object.keys(posts).map(key => (
                        <div className="box" key={key}>
                            <p>Created on: {moment(posts[key].date).format('MMMM Do YYYY, hh:mm:ss a')}</p>
                            <h3>{posts[key].title}</h3>
                            <button>
                                <NavLink to={`/post/${key}`}>Read More</NavLink>
                            </button>
                        </div>
                    ))}
                </>
            )
        }
    }


    return (
        <div className="Post">
            {checkPost()}
        </div>
    );
};

export default Post;